```meta
date: 2022-11-17
```

# mblog

[mblog](https://gitlab.com/krichprollsch/mblog) is a mini static site generator
I wrote in [Go](https://go.dev/) in order to create this blog.
It converts markdown posts into HTML pages.

When I decided to start this blog, I haven't any idea on what to write about.
So it was easier an funnier to code a blog generator and talk a bit about it.

mblog uses the excellent
[Blackfriday](https://pkg.go.dev/github.com/russross/blackfriday/v2) lib to
easily converts markdown into HTML.

It uses also 3 templates to generate the blog you can customize.
For example, the source of blg.tch.re are visible on
https://gitlab.com/krichprollsch/blg.tch.re.
Merge requests are welcome to fix my typos!
