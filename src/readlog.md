```meta
date: 2023-06-30
```

# Read log

## In progress

* https://kristoff.it/blog/what-is-zig-comptime/

## Later

* https://paulgraham.com/hwh.html
* https://basecamp.com/shapeup
* https://mitchellh.com/writing/ghostty-and-useful-zig-patterns
* https://jvns.ca/blog/2023/08/03/behind--hello-world/
* https://tailscale.com/blog/how-nat-traversal-works/
* https://doc.rust-lang.org/book/
* https://ocw.mit.edu/courses/6-006-introduction-to-algorithms-spring-2020/video_galleries/lecture-videos/
* https://ipapi.is/geolocation.html
    * https://news.ycombinator.com/item?id=37507355
    * https://ipinfo.io/blog/probe-network-how-we-make-sure-our-data-is-accurate/
    * https://ipinfo.io/developers/ip-to-country-asn-database
* https://nerdyarticles.com/a-clutter-free-life-with-paperless-ngx/
    * https://docs.paperless-ngx.com
* unikernel
    * https://unikraft.org/
    * https://github.com/nanovms/nanos#getting-started
* https://jvns.ca/blog/2023/11/06/rebasing-what-can-go-wrong-/
* https://tls13.xargs.org/
    * https://tls12.xargs.org/
    * https://quic.xargs.org/
* https://www.theregister.com/2023/12/27/bruce_perens_post_open/
* https://jack-vanlightly.com/blog/2022/1/25/write-for-others-but-mostly-for-yourself
* https://computer.rip/2024-02-25-a-history-of-the-tty.html
* https://www.openmymind.net
* https://matklad.github.io/2023/12/31/O(1)-build-file.html

## Done

### 2024

* 2024-03-31 https://matklad.github.io/2024/03/22/basic-things.html
* 2024-03-07 https://antonz.org/stupid/
* 2024-02-28 Tigerbeetle: The FASTEST and SAFEST Database https://www.youtube.com/watch?v=sC1B3d9C_sI
* 2024-02-27 https://github.com/tigerbeetle/tigerbeetle/blob/main/docs/TIGER_STYLE.md
* 2024-02-24 http://paulgraham.com/greatwork.html
    * https://paulgraham.com/selfindulgence.html
    * https://paulgraham.com/kids.html
    * https://paulgraham.com/users.html
    * https://paulgraham.com/accents.html
* 2024-02-23 https://www.aspiring.dev/fly-io-scheduler-part-1/
* 2024-02-20 https://www.theregister.com/2024/02/21/successor_to_unix_plan_9/
* 2024-02-16 https://www.theregister.com/2024/02/12/drowning_in_code/
* 2024-02-16 https://www.theregister.com/2024/02/16/what_is_unix/
* 2024-02-16 https://yorickpeterse.com/articles/what-it-was-like-working-for-gitlab/
* 2024-02-08 https://moritz.sh/blog/makefiles-fosdem/
* 2014-01-19 Zig's I/O and Concurrency Story - King Protty - Software You Can Love 2022 https://www.youtube.com/watch?v=Ul8OO4vQMTw
* 2024-01-15 https://www.forrestthewoods.com/blog/failing-to-learn-zig-via-advent-of-code/

### 2023
* 2023-12-26 https://github.com/ratfactor/ziglings
* 2023-12-26 https://rampantgames.com/blog/?p=7745
* 2023-12-21 https://siddhesh.substack.com/p/projects
* 2023-12-11 https://hacks.mozilla.org/2021/12/webassembly-and-back-again-fine-grained-sandboxing-in-firefox-95/
    * https://00f.net/2023/12/11/webassembly-compilation-to-c/
* 2023-12-02 https://grugbrain.dev/
* 2023-12-02 https://olano.dev/2023-11-30-code-is-run-more-than-read/
* 2023-11-19 https://www.cst.cam.ac.uk/blog/afb21/oops-we-automated-bullshit
* 2023-11-13 https://www.benkuhn.net/hard/
* 2023-10-30 https://jsoverson.medium.com/was-rust-worth-it-f43d171fb1b3
* 2023-10-23 https://goodenough.us/blog/2023-10-18-good-enough-marketing-the-preparation/
* 2023-10-23 https://flavio.castelli.me/2023/02/07/building-a-unikernel-that-runs-webassembly---part-1/
* 2023-09-27 https://go.dev/doc/modules/layout
* 2023-09-25 https://drewdevault.com/2020/03/18/Reckless-limitless-scope.html
* 2023-09-23 https://www.openmymind.net/learning_zig/
* 2023-09-20 https://xeiaso.net/blog/gokrazy
* 2023-09-18 https://linus.dev/posts/kiesel-devlog-1/
* 2023-09-13 https://devlog.hexops.com/2023/zig-0-11-breaking-build-changes/
* 2023-08-28 https://substrate.tools/blog/terraform-best-practices-for-reliability-at-any-scale
* 2023-08-04 https://www.destroyallsoftware.com/compendium/network-protocols?share_key=97d3ba4c24d21147
* 2023-08-02 https://github.com/ziglang/zig.vim
* 2023-08-02 https://github.com/zigtools/zls
* 2023-08-02 https://ziglearn.org/
* 2023-07-28 https://www.allthingsdistributed.com/2023/07/building-and-operating-a-pretty-big-storage-system.html
* 2023-07-21 https://fly.io/blog/we-raised-a-bunch-of-money/
* 2023-07-21 https://gruchalski.com/tags/firecracker/
* 2023-07-18 https://research.swtch.com/coro
* 2023-07-17 https://hocus.dev/blog/qemu-vs-firecracker/
* 2023-07-13 https://developer.chrome.com/articles/new-headless/
* 2023-07-13 https://jvns.ca/blog/2021/01/23/firecracker--start-a-vm-in-less-than-a-second/
* 2023-07-13 https://github.com/firecracker-microvm/firecracker/blob/main/docs/getting-started.md
* 2023-07-13 https://creatoreconomy.so/p/kaz-coo-shopify-craft-and-no-meetings
* 2023-07-12 https://www.businessinsider.com/tech-industry-fake-work-problem-bad-managers-bosses-layoffs-jobs-2023-7?r=US&IR=T
* 2023-07-07 https://awesomekling.github.io/Excellence-is-a-habit-but-so-is-failure/
* 2023-07-04 https://github.com/ziglang/zig/issues/16270
* 2023-07-04 https://giansegato.com/essays/edutainment-is-not-learning
* 2023-06-30 https://blog.benjojo.co.uk/post/ISPs-sharing-DNS-query-data
* 2023-06-30 https://blog.benjojo.co.uk/post/ip-address-squatting
* 2023-06-30 https://daedtech.com/how-software-groups-rot-legacy-of-the-expert-beginner/
* 2023-06-30 http://brucefwebster.com/2008/04/11/the-wetware-crisis-the-dead-sea-effect/
* 2023-06-30 https://daedtech.com/how-to-keep-your-best-programmers/
* 2023-06-30 https://daedtech.com/how-developers-stop-learning-rise-of-the-expert-beginner/

