.PHONY: build install

build:
	@mblog --tmpl tmpl --in src --out public

install:
	@mkdir -p public/css
	@curl -s -o public/css/bahunya.min.css 'https://cdn.jsdelivr.net/gh/kimeiga/bahunya/dist/bahunya.min.css'

deploy: build
	@DOCKER_HOST="ssh://uma" docker exec -ti caddy mkdir -p /var/www/blg.tch.re
	@DOCKER_HOST="ssh://uma" docker cp public caddy:/var/www/blg.tch.re
